This repository contains code for calling SNPs on the 2018 Genomes to Fields inbred lines.  Genotyping was performed using the Practical Haplotype Graph (PHG; https://bitbucket.org/bucklerlab/practicalhaplotypegraph/wiki/Home).

Documentation about the current (alpha) release of the SNP dataset can be found here:
https://docs.google.com/document/d/1my8vrsooG_ZS3VvtH46VN84S3GeY-3nTLDjxO9-pMcA/edit?usp=sharing

Contact:
Joseph Gage - jlg374@cornell.edu
Cinta Romay - mcr72@cornell.edu

