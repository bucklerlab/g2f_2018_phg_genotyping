G2F 2018 Genotypic Data

** Please note that this represents a PRELIMINARY release of the 2018 G2F
   inbred genotypic data. There are errors in this dataset, and it is
   unfiltered in any way.  Please perform your own quality control before
   using it.  We expect to replace this with an improved version before the
   official DOI release.

1. G2F_2018_PHG_alpha_description_2019-05-03.pdf
   Description of sequencing, SNP calling, and SNP data quality evaluation

2. G2F_2018_PHG_alpha.vcf.gz
   SNP data in compressed VCF format. 
   Note: Mac and Linux OS's can extract this file type natively, Windows OS 
         requires software to extract the archive.  7-Zip 
         (http://www.7-zip.org) is free and open source.

3. G2F_2018_PHG_alpha_samples.txt
   File to connect sample names to inbred genotype names.

Raw sequencing reads will be available under BioProject ID PRJNA530187 on NCBI.

Code for creating the files above can be found at:
  https://bitbucket.org/bucklerlab/g2f_2018_phg_genotyping/src/master/





