#!/bin/bash

mkdir /workdir/jlg374
cd /workdir/jlg374

# Setup files and programs for the new HMM and minimap2 alignment methods
scp -r jlg374@cbsumm12.tc.cornell.edu:/workdir/zrm22/KmerTests/Run282MinimapPipeline/minimap2 ./
scp jlg374@cbsumm12.tc.cornell.edu:/workdir/zrm22/KmerTests/Run282MinimapPipeline/haplotypesMinusGDB_k21_w11_I_90G.mmi ./
cp -r ~/FilesForJoe/Tassel_PHG ./
cp ~/phg.jar ./Tassel_PHG/tassel-5-standalone/lib/phg.jar

# Get fastq files
iget -rV /ibl/home/RawSeqData/WGS/Zea/Novo_GxE &
mkdir reads_for_PHG
# To get all fastq:
mv Novo_GxE/C202*/raw_data/*18N* ./reads_for_PHG/
# To get test subset:
# SAMPLES=(GE18N437 GE18N434 GE18N001 GE18N433 GE18N436)
# for SAMPLE in ${SAMPLES[@]}
# do
#   FILE=$( find ./Novo_GxE/ -name $SAMPLE*fq.gz )
#   DIR=$( dirname -- "$FILE" )
#   mv $DIR ./reads_for_PHG/$(basename $DIR)
# done
