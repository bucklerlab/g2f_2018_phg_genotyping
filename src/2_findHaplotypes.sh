DIRS=$( ls reads_for_PHG/ )
OUTPUT=Haplotypes/

CONFIG=./Tassel_PHG/config.txt

mkdir -p $OUTPUT
mkdir -p ./Logs/

for DIR in $DIRS
do
  READPATH=./reads_for_PHG/$DIR
  LOGFILE=./Logs/findHaplotypes_${DIR}.log
  time ./Tassel_PHG/tassel-5-standalone/run_pipeline.pl -debug -Xmx250g -HaplotypeGraphBuilderPlugin -configFile $CONFIG -methods mummer4 -includeVariantContexts false -includeSequences false -includeEdges true -endPlugin -FastqDirToMappingPlugin -minimap2IndexFile haplotypesMinusGDB_k21_w11_I_90G.mmi -fastqDir $READPATH/ -mappingFileDir $OUTPUT/ > $LOGFILE 2>&1
done
