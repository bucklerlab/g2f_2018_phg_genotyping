#!/bin/bash 

MINREADS=1

HAPDIR=/workdir/jlg374/Haplotypes
OUTDIR=/workdir/jlg374/Paths_minreads${MINREADS}
LOGFILE=/workdir/jlg374/Logs/findPaths_minreads${MINREADS}.log
CONFIG=./Tassel_PHG/config_minreads${MINREADS}.txt

mkdir -p $OUTDIR

time ./Tassel_PHG/tassel-5-standalone/run_pipeline.pl -debug -Xmx250g -HaplotypeGraphBuilderPlugin -configFile $CONFIG -methods mummer4,refRegionGroup -includeVariantContexts false -includeSequences false -endPlugin -HapCountBestPathToTextPlugin -configFile $CONFIG -inclusionFileDir $HAPDIR -outputDir $OUTDIR -hapCountMethod HAPCOUNT_METHOD_1 -pMethod PATH_METHOD_1 > $LOGFILE 2>&1
