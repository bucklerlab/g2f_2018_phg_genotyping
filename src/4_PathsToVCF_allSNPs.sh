#!/bin/bash -x

echo "Starting minreads=1"

MINREADS=1
CONFIG=Tassel_PHG/config_minreads${MINREADS}.txt
REF=Zea_mays.AGPv4.dna.toplevelMtPtv3.fa

PATHS=/workdir/jlg374/Paths_minreads${MINREADS}
OUTPUT=/workdir/jlg374/G2F_pairedEnd_all_minreads${MINREADS}.vcf

time Tassel_PHG/tassel-5-standalone/run_pipeline.pl -Xmx250g -debug -HaplotypeGraphBuilderPlugin -configFile $CONFIG -methods mummer4,refRegionGroup -includeVariantContexts true -endPlugin -ImportHaplotypePathFilePlugin -inputFileDirectory $PATHS -endPlugin -PathsToVCFPlugin -ref ${REF} -outputFile $OUTPUT -endPlugin > Logs/pathsToVCF_G2F_all_minreads${MINREADS}.log 2>&1

wait

echo "Starting minreads=0"

MINREADS=0
CONFIG=Tassel_PHG/config_minreads${MINREADS}.txt
REF=Zea_mays.AGPv4.dna.toplevelMtPtv3.fa

PATHS=/workdir/jlg374/Paths_minreads${MINREADS}
OUTPUT=/workdir/jlg374/G2F_pairedEnd_all_minreads${MINREADS}.vcf

time Tassel_PHG/tassel-5-standalone/run_pipeline.pl -Xmx250g -debug -HaplotypeGraphBuilderPlugin -configFile $CONFIG -methods mummer4,refRegionGroup -includeVariantContexts true -endPlugin -ImportHaplotypePathFilePlugin -inputFileDirectory $PATHS -endPlugin -PathsToVCFPlugin -ref ${REF} -outputFile $OUTPUT -endPlugin > Logs/pathsToVCF_G2F_all_minreads${MINREADS}.log 2>&1
