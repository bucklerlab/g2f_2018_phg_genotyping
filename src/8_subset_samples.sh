#!/bin/bash

VCF=/media/jlg374/Data1/G2F_SNPs/G2F_pairedEnd_all_minreads1.vcf

# Get list of all Taxa
head $VCF | tail -1 | sed 's/\t/\n/g' | tail -n +10 | sort > all_taxa.txt

# Remove all 'CT18' taxa
grep "CT18N" all_taxa.txt > remove_taxa.txt

# Remove all samples with number greater than 438
CUTOFF=$( grep -n "GE18N438" all_taxa.txt | cut -f1 -d:)
tail -n +$( echo ${CUTOFF} + 1 | bc ) all_taxa.txt >> remove_taxa.txt

# Remove taxa with TASSEL
VCFOUT=/media/jlg374/Data1/G2F_SNPs/G2F_2018_PHG_alpha.vcf
~/applications/tassel-5-standalone/run_pipeline.pl -Xmx50g \
  -vcf $VCF \
  -excludeTaxaInFile remove_taxa.txt \
  -export $VCFOUT \
  -exportType VCF \
  -exportIncludeAnno false \
  -exportIncludeDepth false

# Tassel appends a '1' to the filename - this removes it
mv $( echo $VCFOUT | sed 's/.vcf/1.vcf/' ) $VCFOUT

# Rename samples
sed -i 's/_multimap.txt//g' $VCFOUT

# Make sample decoder file
DECODER=/media/jlg374/Data1/G2F_SNPs/phg_decoder.txt
NEWDECODER=/media/jlg374/Data1/G2F_SNPs/G2F_2018_PHG_alpha_samples.txt
head -11 $VCFOUT | tail -1 | sed 's/\t/\n/g' | tail -n +10 > samples.txt
echo -e "Sample\tID\tInbred" > $NEWDECODER
grep -f samples.txt $DECODER >> $NEWDECODER
sed -i 's/^countFile_//' $NEWDECODER
sed -i 's/_1\t/\t/' $NEWDECODER

# Split original geno file into per-chrom files
~/applications/tassel-5-standalone/run_pipeline.pl -Xmx50g \
  -vcf $VCFOUT \
  -separate \
  -export \
  -exportType VCF \
  -exportIncludeAnno false \
  -exportIncludeDepth false

