#!/bin/bash

GENO=/media/jlg374/Data1/G2F_SNPs/G2F_2018_PHG_alpha.vcf
OUT=~/projects/G2F_genotyping/dat/G2F_2018_PHG_alpha_distance.txt

~/applications/tassel-5-standalone/run_pipeline.pl -debug -Xmx50g \
  -vcf $GENO \
  -filterAlign -filterAlignMinCount 10 -filterAlignMinFreq 0.0001 -filterAlignRemMinor \
  -distanceMatrix \
  -export $OUT

